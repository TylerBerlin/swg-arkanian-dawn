heavy_weapons_rifle = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "rifle_acid_beam", weight = 2000000},
		{itemTemplate = "rifle_flame_thrower", weight = 2500000},
		{itemTemplate = "rifle_flame_thrower_cold", weight = 3000000},
		{itemTemplate = "rifle_lightning", weight = 2500000}
	}
}

addLootGroupTemplate("heavy_weapons_rifle", heavy_weapons_rifle)
