drall_patriot_fluffy_munchkins = Creature:new {
	objectName = "@mob/creature_names:drall_patriot_fluffy_munchkins",
	customName = "Fluffy Munchkins (a Drall Patriot)",
	socialGroup = "drall",
	faction = "drall",
	level = 200,
	chanceHit = 100,
	damageMin = 2200,
	damageMax = 2600,
	baseXp = 3000,
	baseHAM = 90000,
	baseHAMmax = 120000,
	armor = 3,
	resists = {100,100,65,65,65,65,65,65,75},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 80,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/drall_male.iff"},
	lootGroups = {
          {
            groups = {
              {group = "junk_rare", chance = 10000000},
            },
            lootChance = 10000000,
          },
          {
						groups = {
              {group = "junk_rare", chance = 10000000},
            },
            lootChance = 10000000,
          },
        },
	scale = 2.4,
	weapons = {"gamorrean_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster,swordsmanmaster)
}

CreatureTemplates:addCreatureTemplate(drall_patriot_fluffy_munchkins, "drall_patriot_fluffy_munchkins")
