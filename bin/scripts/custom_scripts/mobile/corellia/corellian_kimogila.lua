corellian_kimogila = Creature:new {
	objectName = "@mob/creature_names:enraged_kimogila",
	customName = "a Native Corellian Kimogila",
	socialGroup = "kimogila",
	faction = "",
	level = 175,
	chanceHit = 92.5,
	damageMin = 1200,
	damageMax = 1980,
	baseXp = 14884,
	baseHAM = 96000,
	baseHAMmax = 118000,
	armor = 2,
	resists = {130,145,155,155,145,30,30,30,-1},
  meatType = "meat_carnivore",
	meatAmount = 1000,
	hideType = "hide_leathery",
	hideAmount = 1000,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 25,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/kimogila_hatchling.iff"},
  scale = 0.5,
	lootGroups = {
    {
			groups = {
				{group = "acklay", chance = 10000000}
			},
      lootChance = 10000000,
		},
    {
			groups = {
				{group = "kimogila_common", chance = 7000000},
				{group = "giant_dune_kimo_common", chance = 3000000}
			},
      lootChance = 10000000,
		},
	},
	weapons = {},
	conversationTemplate = "",
	attacks = {
		{"posturedownattack","stateAccuracyBonus=50"},
		{"creatureareacombo","stateAccuracyBonus=50"},
    {"knockdownattack",""},
    {"dizzyattack",""}
	}
}

CreatureTemplates:addCreatureTemplate(corellian_kimogila, "corellian_kimogila")
