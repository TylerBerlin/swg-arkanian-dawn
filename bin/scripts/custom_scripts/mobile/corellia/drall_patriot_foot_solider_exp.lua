drall_patriot_foot_soldier_exp = Creature:new {
	objectName = "@mob/creature_names:drall_patriot_footsoldier",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "drall",
	faction = "drall",
	level = 110,
	chanceHit = 100,
	damageMin = 1200,
	damageMax = 2860,
	baseXp = 2600,
	baseHAM = 60000,
	baseHAMmax = 63000,
	armor = 2,
	resists = {100,100,20,20,20,20,20,20,20},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 80,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {
		"object/mobile/drall_male.iff",
		"object/mobile/drall_female.iff"},
	lootGroups = {
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	        },
	scale = 1.3,
	weapons = {"primitive_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster,pikemanmaster,fencermaster)
}

CreatureTemplates:addCreatureTemplate(drall_patriot_foot_soldier_exp, "drall_patriot_foot_soldier_exp")
