includeFile("../custom_scripts/mobile/corellia/carrion_spat_chicken_nugget.lua")
includeFile("../custom_scripts/mobile/corellia/corellian_kimogila.lua")
includeFile("../custom_scripts/mobile/corellia/corellian_kimogila_lesser.lua")
includeFile("../custom_scripts/mobile/corellia/righthand_lord_nyax.lua")

-- Drall Cave
includeFile("../custom_scripts/mobile/corellia/drall_patriot_exp.lua")
includeFile("../custom_scripts/mobile/corellia/drall_patriot_conqueror_exp.lua")
includeFile("../custom_scripts/mobile/corellia/drall_patriot_foot_solider_exp.lua")
includeFile("../custom_scripts/mobile/corellia/drall_patriot_legionnaire_exp.lua")
includeFile("../custom_scripts/mobile/corellia/drall_patriot_pygmy_exp.lua")
includeFile("../custom_scripts/mobile/corellia/drall_patriot_fluffy_munchkins.lua")
