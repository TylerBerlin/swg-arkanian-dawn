drall_patriot_legionnaire_exp = Creature:new {
	objectName = "@mob/creature_names:drall_patriot_legionnare",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "drall",
	faction = "drall",
	level = 70,
	chanceHit = 40,
	damageMin = 3400,
	damageMax = 3960,
	baseXp = 2000,
	baseHAM = 40000,
	baseHAMmax = 56000,
	armor = 0,
	resists = {20,20,20,20,20,20,20,-1,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 80,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {
		"object/mobile/drall_male.iff",
		"object/mobile/drall_female.iff"},
	lootGroups = {
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	          {
							groups = {
	              {group = "junk", chance = 9850000},
								{group = "junk_rare", chance = 100000},
								{group = "junk_rare", chance = 50000},
	            },
	            lootChance = 10000000,
	          },
	        },
	scale = 0.8,
	weapons = {"primitive_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster,pikemanmaster,fencermaster)
}

CreatureTemplates:addCreatureTemplate(drall_patriot_legionnaire_exp, "drall_patriot_legionnaire_exp")
