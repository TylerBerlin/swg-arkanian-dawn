drall_patriot_conqueror_exp = Creature:new {
	objectName = "@mob/creature_names:drall_patriot_conqueror",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "drall",
	faction = "drall",
	level = 100,
	chanceHit = 100,
	damageMin = 1200,
	damageMax = 1860,
	baseXp = 3000,
	baseHAM = 18000,
	baseHAMmax = 19000,
	armor = 3,
	resists = {90,90,90,90,90,90,90,90,95},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 80,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/drall_male.iff"},
	lootGroups = {
          {
						groups = {
              {group = "junk", chance = 9850000},
							{group = "junk_rare", chance = 100000},
							{group = "junk_rare", chance = 50000},
            },
            lootChance = 10000000,
          },
          {
						groups = {
              {group = "junk", chance = 9850000},
							{group = "junk_rare", chance = 100000},
							{group = "junk_rare", chance = 50000},
            },
            lootChance = 10000000,
          },
          {
						groups = {
              {group = "junk", chance = 9850000},
							{group = "junk_rare", chance = 100000},
							{group = "junk_rare", chance = 50000},
            },
            lootChance = 10000000,
          },
          {
						groups = {
              {group = "junk", chance = 9850000},
							{group = "junk_rare", chance = 100000},
							{group = "junk_rare", chance = 50000},
            },
            lootChance = 10000000,
          },
        },
	weapons = {"gamorrean_weapons"},
	conversationTemplate = "",
	reactionStf = "@npc_reaction/military",
	attacks = merge(brawlermaster,swordsmanmaster)
}

CreatureTemplates:addCreatureTemplate(drall_patriot_conqueror_exp, "drall_patriot_conqueror_exp")
