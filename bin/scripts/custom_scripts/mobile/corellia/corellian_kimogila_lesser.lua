corellian_kimogila_lesser = Creature:new {
	objectName = "@mob/creature_names:enraged_kimogila",
	customName = "a Lesser Corellian Kimogila",
	socialGroup = "kimogila",
	faction = "",
	level = 90,
	chanceHit = 92.5,
	damageMin = 500,
	damageMax = 620,
	baseXp = 3884,
	baseHAM = 64000,
	baseHAMmax = 72000,
	armor = 1,
	resists = {-1,-1,0,0,0,0,0,0,-1},
  meatType = "meat_carnivore",
	meatAmount = 0,
	hideType = "hide_leathery",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 25,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/kimogila_hatchling.iff"},
  scale = 0.2,
	lootGroups = { },
	weapons = {"creature_spit_small_yellow"},
	conversationTemplate = "",
	attacks = {
		{"creatureareapoison",""},
	}
}

CreatureTemplates:addCreatureTemplate(corellian_kimogila_lesser, "corellian_kimogila_lesser")
