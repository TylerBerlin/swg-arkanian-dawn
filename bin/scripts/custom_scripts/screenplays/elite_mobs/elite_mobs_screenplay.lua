EliteMobsScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "EliteMobsScreenPlay",

	spawnTimers = {
		initial = { 0.1, 0.1 }, -- Time in minutes for first spawn after server start
		respawn = { 50, 70 }, -- Time in minutes before respawning, Dead mob respawn are up to 2x this rate.
	},

	spawnMap = {
		{ planet = "corellia", mobile = "corellian_kimogila", spawnX = 4400, spawnY = -4000, spawnOffset = 32 },
		{ planet = "corellia", mobile = "corellian_kimogila", spawnX = 4400, spawnY = -4000, spawnOffset = 64 },
		{ planet = "corellia", mobile = "righthand_lord_nyax", spawnX = 4400, spawnY = -4000, spawnOffset = 32 },
		--{ planet = "lok", mobile = "righthand_lord_nyax", spawnX = 4400, spawnY = -4000, spawnOffset = 32 },
	},
}
--[[ 	Random Mob Spawn ScreenPlay
	  - Set respawn timer to the tic rate you wish the screenplay to respawn idle mobs
		- The respawn a of dead mobs will be anywhere between double the tic rate and the tic rate itself
]]--

registerScreenPlay("EliteMobsScreenPlay", true)

function EliteMobsScreenPlay:start()
	self:spawnAnchor()
	self:initEvents()
end

function EliteMobsScreenPlay:spawnAnchor()
	local pAnchor = spawnMobile("corellia", "mouse_droid", 0, 3320.8, 324, 5712.2, 179, 0)
	writeData("elite_mobs:anchor_id", SceneObject(pAnchor):getObjectID())
end

function EliteMobsScreenPlay:initEvents()
	if (not hasServerEvent("EliteMobEvent")) then
		self:startEliteMob("initial")
	end
end

function EliteMobsScreenPlay:startEliteMob(event)
	createServerEvent(self:getEventTimer(event), "EliteMobsScreenPlay", "doEliteMobCheck", "EliteMobEvent")
end

function EliteMobsScreenPlay:getEventTimer(event)
	local timer = self.spawnTimers[event]

	if (timer == nil) then
		return getRandomNumber(20, 30) * 60 * 1000
	else
		return getRandomNumber(timer[1], timer[2]) * 60 * 1000
	end
end

function EliteMobsScreenPlay:getSpawnRandom(offset,loc)
	return getRandomNumber(offset*-1,offset) + loc
end

function EliteMobsScreenPlay:destroyMobSpawn(mobSceneObject, eliteMob, i)
	SceneObject(mobSceneObject):destroyObjectFromWorld()
	deleteData(eliteMob.mobile .. i .. ":mobId")
	deleteData(eliteMob.mobile .. i .. ":mobAlive")
end

function EliteMobsScreenPlay:createEliteMobSpawn(pAnchor, eliteMob, i)
	local newLoc = {
		self:getSpawnRandom(eliteMob.spawnOffset, eliteMob.spawnX),
		self:getSpawnRandom(eliteMob.spawnOffset, eliteMob.spawnY)
	}
	local spawnZ = getTerrainHeight(pAnchor, newLoc[1], newLoc[2])
	local pEliteMob = spawnMobile(eliteMob.planet, eliteMob.mobile, 0, newLoc[1], spawnZ, newLoc[2], getRandomNumber(360), 0)

	writeData(eliteMob.mobile .. i .. ":mobId", SceneObject(pEliteMob):getObjectID())
	writeData(eliteMob.mobile .. i .. ":mobAlive", 1)
end

-- intial spawn, runs once
function EliteMobsScreenPlay:createEliteMobs(pAnchor)
	for i,eliteMob in pairs(self.spawnMap) do
		if (isZoneEnabled(eliteMob.planet)) then
			self:createEliteMobSpawn(pAnchor, eliteMob, i)
		end
	end
	writeData("elite_mobs:started", 1)
end

function EliteMobsScreenPlay:doEliteMobCheck()
	local eventStarted = readData("elite_mobs:started")
	local anchorId = readData("elite_mobs:anchor_id")
	local pAnchor = getSceneObject(anchorId)

	if (eventStarted ~= 1) then
		-- initial spawn
		self:createEliteMobs(pAnchor)
	else
		for i,eliteMob in pairs(self.spawnMap) do
			if (isZoneEnabled(eliteMob.planet)) then
				local mobSceneObject = getSceneObject(readData(eliteMob.mobile .. i .. ":mobId"))
				local mobAlive = readData(eliteMob.mobile .. i .. ":mobAlive")

				if (CreatureObject(mobSceneObject):isDead() and mobAlive == 1) then
					-- mob has died between checks, Mark as deceased, Allows time for players to loot corpse
					writeData(eliteMob.mobile .. i .. ":mobAlive", 0)
				elseif (AiAgent(mobSceneObject):isInCombat() == false or mobAlive == 0) then
					-- mob is not in combat or has previously been marked as deceased, Destroy and respawn
					self:destroyMobSpawn(mobSceneObject, eliteMob, i)
					self:createEliteMobSpawn(pAnchor, eliteMob, i)
				end
			end
		end
	end
	self:startEliteMob("respawn")
end
