-- containers
includeFile("../custom_scripts/loot/items/junk/drum_storage_container.lua")
includeFile("../custom_scripts/loot/items/junk/plain_crate_01_container.lua")

-- junk generic
includeFile("../custom_scripts/loot/items/junk/adv_vehicle_module_junk.lua")
includeFile("../custom_scripts/loot/items/junk/adv_vehicle_structure_frame_junk.lua")
includeFile("../custom_scripts/loot/items/junk/disperser_junk.lua")
includeFile("../custom_scripts/loot/items/junk/extended_fuel_tanks_junk.lua")
includeFile("../custom_scripts/loot/items/junk/mass_driver_junk.lua")
includeFile("../custom_scripts/loot/items/junk/launcher_pod_junk.lua")
includeFile("../custom_scripts/loot/items/junk/laser_cannon_junk.lua")
includeFile("../custom_scripts/loot/items/junk/proton_torpedo_array_junk.lua")
includeFile("../custom_scripts/loot/items/junk/storage_bay_junk.lua")
includeFile("../custom_scripts/loot/items/junk/weapon_array_junk.lua")
includeFile("../custom_scripts/loot/items/junk/weapon_link_junk.lua")
includeFile("../custom_scripts/loot/items/junk/blaster_cannon_junk.lua")

-- junk misc
includeFile("../custom_scripts/loot/items/junk/moldy_plant_junk.lua")
