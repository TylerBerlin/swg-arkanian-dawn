-- www.revelationonline.net
-- Author: matthias.muente@gmx.de

enhanced_release_mechanism = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Enhanced Release Mechanism",
	directObjectTemplate = "object/tangible/component/chemistry/release_mechanism_duration_advanced.iff",
	craftingValues = {
		{"power",80,80,0},
		{"useCount",2,2,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("enhanced_release_mechanism", enhanced_release_mechanism)
