-- www.revelationonline.net
-- Original Author: matthias.muente@gmx.de

exceptional_turbo_fluidic_drilling_pumping_unit = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Turbo Fluidic Drilling Pump",
	directObjectTemplate = "object/tangible/component/structure/turbo_fluidic_drilling_pumping_unit.iff",
	craftingValues = {
		{"hitpoints",2500,2500,0},
		{"extractrate",16,16,0},
		{"useCount",2,2,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("exceptional_turbo_fluidic_drilling_pumping_unit", exceptional_turbo_fluidic_drilling_pumping_unit)
