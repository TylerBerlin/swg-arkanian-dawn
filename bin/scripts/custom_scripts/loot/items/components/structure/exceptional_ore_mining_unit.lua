-- www.revelationonline.net
-- Original Author: matthias.muente@gmx.de

exceptional_ore_mining_unit = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Ore Mining Unit",
	directObjectTemplate = "object/tangible/component/structure/ore_mining_unit.iff",
	craftingValues = {
		{"hitpoints",2500,2500,0},
		{"extractrate",16,16,0},
		{"useCount",2,2,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("exceptional_ore_mining_unit", exceptional_ore_mining_unit)
