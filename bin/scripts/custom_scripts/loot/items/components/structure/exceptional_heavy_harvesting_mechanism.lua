-- www.revelationonline.net
-- Original Author: matthias.muente@gmx.de

exceptional_heavy_harvesting_mechanism = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "Exceptional Heavy Harvesting Mechanism",
	directObjectTemplate = "object/tangible/component/structure/heavy_harvesting_mechanism.iff",
	craftingValues = {
		{"hitpoints",2500,2500,0},
		{"extractrate",16,16,0},
		{"useCount",2,2,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("exceptional_heavy_harvesting_mechanism", exceptional_heavy_harvesting_mechanism)
